(define-module (petanqulator)
  #:use-module (guix gexp) 
  #:use-module (guix packages) 
  #:use-module (gnu packages algebra) 
  #:use-module (gnu packages cmake) 
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages game-development)
  #:use-module (guix build-system cmake)
  #:use-module (guix licenses))

(define-public petanqulator
  (package
    (name "petanqulator")
    (version "0.1")
    (source (local-file "" #:recursive? #t))
    (build-system cmake-build-system)
    (native-inputs `(("pkg-config" ,pkg-config)))
    (inputs `(("bullet" ,bullet)
              ("eigen" ,eigen)
              ))
    (arguments '(#:configure-flags '("-DGUI=OFF")  ; no gtkglextmm in guix
                 #:tests? #f)) 
    (synopsis "petanqulator")
    (description "petanqulator")
    (home-page "https://github.com/juliendehos/petanqulator")
    (license bsd-3)))


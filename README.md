# petanqulator 
[![Build status](https://gitlab.com/juliendehos/petanqulator/badges/master/build.svg)](https://gitlab.com/juliendehos/petanqulator/pipelines) 

[PETANQue](https://en.wikipedia.org/wiki/P%C3%A9tanque) simULATOR 

![](doc/screenshot_gui.png)

## Dependencies

- bullet physics
- gtkmm, gtkglextmm
- opengl, glu
- cxxtest
- gcc >= 4.9
- cmake

## Build (cmake)

```
mkdir build_cmake
cd build_cmake
cmake ..
# cmake -DCMAKE_BUILD_TYPE=Debug ..
# CXXFLAGS="-DLOG" cmake -DCMAKE_BUILD_TYPE=Debug ..
make 
```

## Build (meson)

```
mkdir build_meson
cd build_meson
meson ..
ninja
```

## Build (tup)

```
tup init
tup
```

## Build (nix)

```
nix-build
```


## Build (guix)

```
GUIX_PACKAGE_PATH=`pwd` guix package -i petanqulator
```


## TODO

- discard out-limit balls
- implement a more interesting terrain
- render ball rotation in OpenGL view (+ rolling friction)
- use a single collision shape + multiples collision objects (for balls)


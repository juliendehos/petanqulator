with import <nixpkgs> {}; 

stdenv.mkDerivation {
    name = "petanqulator";
    buildInputs = [ stdenv cmake bullet xorg.libXmu pkgconfig 
        gnome2.gtkglextmm gnome2.gtkmm xorg.libxcb xorg.libpthreadstubs eigen 
        pcre cxxtest 
        mesa_noglu ];
        #mesa-glu ];
    src = ./.;

}

